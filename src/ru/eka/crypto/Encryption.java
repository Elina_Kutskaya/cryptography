package ru.eka.crypto;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.KeyGenerator;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;
import java.util.Scanner;

/**
 * Класс для шифрования/дешифрования изображения
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class Encryption {
    private static Cipher cipher;

    /**
     * Метод шифрования изображения
     *
     * @param image путь к изображению
     * @param secret ключ шифрования
     * @return строку с зашифрованным массивом байт
     * @throws Exception
     */
    private static String encrypt(String image, SecretKey secret) throws Exception {
        BufferedImage originalImage = ImageIO.read(new File(image));
        ByteArrayOutputStream byteArr = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "jpg", byteArr);
        byteArr.flush();
        cipher.init(Cipher.ENCRYPT_MODE, secret);
        byte[] encryptedByte = cipher.doFinal(byteArr.toByteArray());
        byteArr.close();
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(encryptedByte);
    }

    /**
     * Метод дешифрования изображения
     *
     * @param encryptImage строка с зашифрованным массивом байт
     * @param secret ключ шифрования
     * @throws Exception
     */
    private static void decrypt(String encryptImage, SecretKey secret) throws Exception {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] encryptedByte = decoder.decode(encryptImage);
        cipher.init(Cipher.DECRYPT_MODE, secret);
        byte[] decryptedByte = cipher.doFinal(encryptedByte);
        InputStream in = new ByteArrayInputStream(decryptedByte);
        BufferedImage decryptImage = ImageIO.read(in);
        ImageIO.write(decryptImage, "jpg", new File("img2.jpg"));
    }

    /**
     * Метод для записи шифра в файл
     *
     * @param encrypt строка с зашифрованным массивом байт
     */
    private static void saveCipherToAFile(String encrypt) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("img.txt", true))) {
            bufferedWriter.write(encrypt);
        } catch (IOException ignored) {
        }
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            KeyGenerator key = KeyGenerator.getInstance("AES");
            key.init(128);
            SecretKey secret = key.generateKey();
            cipher = Cipher.getInstance("AES");
        System.out.println("Выберите действие:");
        System.out.println("1-шифрование,2-дешифрование");
        int choice = scanner.nextInt();
        switch (choice){
            case 1:
                System.out.println("Введите путь к изображению: ");
                //F:/Work/Cryptography/src/img.jpg
                String image = scanner.next();
                String encryptImage = encrypt(image, secret);
                System.out.println("Шифрование завершено!");
                saveCipherToAFile(encryptImage);
                System.out.println("Шифр сохранен в файл...");
                break;
            case 2:
                System.out.println("Введите зашифрованную строку: ");
                String saveCipherToAFile = scanner.next();
                decrypt(saveCipherToAFile, secret);
                System.out.println("Дешифрование завершено!");
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}